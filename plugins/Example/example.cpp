/*
 * Copyright (C) 2022  Marcel Alexandru Nitan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * diabloweb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QFile>
#include <QDir>

#include "example.h"

Example::Example() {

}

bool Example::gameDataExists() {
    QFile gameData("/home/phablet/.local/share/diabloweb.nitanmarcel/diablo/DIABDAT.MPQ");
    return gameData.exists();
}

void Example::createDiabloFolder() {
    QDir diabloFolder("/home/phablet/.local/share/diabloweb.nitanmarcel/diablo/");
    if (!diabloFolder.exists())
        diabloFolder.mkpath(".");
}

void Example::createDownloadFolder() {
    QDir downloadFolder("/home/phablet/.local/share/diabloweb.nitanmarcel/diablo/saves");
    if (!downloadFolder.exists())
        downloadFolder.mkpath(".");
}
